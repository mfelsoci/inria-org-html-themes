;; Publish the contents of this repository as a HTML page.
;; Source: Rasmus

(require 'package)
(package-initialize)
(add-to-list 'package-archives '("org" . "https://orgmode.org/elpa/") t)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(package-refresh-contents)
(package-install 'org-plus-contrib)
(package-install 'htmlize)

(require 'org)
(require 'ox-publish)

(defvar site-attachments
  (regexp-opt '("setup" "jpg" "jpeg" "gif" "png" "svg"
                "ico" "css" "js" "woff" "html" "pdf"))
  "File types that are published as static files.")

;; Force publishing of unchanged files. Otherwise, missing files cause
;; 'Not Found' errors while browsing the generated website.
(setq org-publish-use-timestamps-flag nil)

(setq org-publish-project-alist
      (list
       (list "site-org" ;; Publish the example page as HTML. 
             :base-directory "."
             :base-extension "org"
             :recursive t
             :publishing-function '(org-html-publish-to-html)
             :publishing-directory "./public"
             :html-head-extra "<link rel=\"icon\" type=\"image/x-icon\"
href=\"https://mfelsoci.gitlabpages.inria.fr/inria-org-html-themes/favicon.ico\"
/>")
       (list "site-static" ;; Publish the sources of the theme.
             :base-directory "."
             :exclude "public/"
             :base-extension site-attachments
             :recursive t
             :publishing-directory "./public"
             :publishing-function 'org-publish-attachment
             :recursive t)
       (list "site" :components '("site-org"))))

(provide 'publish)
